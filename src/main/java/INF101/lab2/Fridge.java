package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge{

    ArrayList<FridgeItem> ItemsInFridge = new ArrayList<>();
    int max_size = 20;

    @Override
    public int totalSize() {
        return max_size;
    }
    
    @Override
    public int nItemsInFridge() {
        return ItemsInFridge.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (ItemsInFridge.size()>=max_size){
            return false;
        }
        else{
            ItemsInFridge.add(item);
            return true;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (ItemsInFridge.size()<1) {
            throw new NoSuchElementException();
        }
        ItemsInFridge.remove(item);
    }

    @Override
    public void emptyFridge() {
        ItemsInFridge.removeAll(ItemsInFridge);   
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expList = new ArrayList<>();
        for (int i = 0; i < ItemsInFridge.size() ; i++) {
            if (ItemsInFridge.get(i).hasExpired()){
                expList.add(ItemsInFridge.get(i));
            }

            else{
                continue;
            }
        }

        ItemsInFridge.removeAll(expList);
        return expList;
    }





    







}
    

